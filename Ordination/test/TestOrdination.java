package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Ordination;

public class TestOrdination {

	@Test
	public void antalDageTest() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 1, 1, 1, 1);

		assertEquals(2, d1.antalDage());
	}

	@Test
	public void antalDageTest2() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 18), 1, 1, 1, 1);

		assertEquals(18, d1.antalDage());
	}

	@Test
	public void antalDageTest3() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 02, 21), 1, 1, 1, 1);

		assertEquals(33, d1.antalDage());
	}

}
