package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;

public class DagligFastTest {

	@Test
	public void TestCreateDagligFast() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 1, 1, 1, 1);

		assertNotNull(d1);
		assertEquals(LocalDate.of(2020, 01, 20), d1.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 21), d1.getSlutDen());
		double tal = 4;
		assertEquals(tal, d1.doegnDosis(), 0.01);

	}

	@Test
	public void TestCreateDagligFast2() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 01), LocalDate.of(2020, 01, 12), 1, 1, 1, 1);

		assertNotNull(d1);
		assertEquals(LocalDate.of(2020, 01, 01), d1.getStartDen());
		assertEquals(LocalDate.of(2020, 01, 12), d1.getSlutDen());
		assertEquals(48.0, d1.samletDosis(), 0.01);

	}

	@Test
	public void TestDoegnDosis() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 1, 1, 1, 1);

		assertEquals(4, d1.doegnDosis(), 0.01);

	}

	@Test
	public void TestDoegnDosis2() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 0, 0, 0, 0);

		assertEquals(0, d1.doegnDosis(), 0.01);

	}

	@Test
	public void TestDoegnDosis3() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 2, 0, 0, 2);

		assertEquals(4, d1.doegnDosis(), 0.01);

	}

	@Test
	public void TestDoegnDosis4() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), 10, 10, 10, 20);

		assertEquals(50.0, d1.doegnDosis(), 0.01);

	}

	@Test
	public void TestSamletDosis() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 20), 1, 1, 1, 1);

		assertEquals(4, d1.samletDosis(), 0.01);

	}

	@Test
	public void TestSamletDosis2() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 20), 0, 0, 0, 0);

		assertEquals(0, d1.samletDosis(), 0.01);

	}

	@Test
	public void TestSamletDosis3() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 20), 2, 2, 2, 3);

		assertEquals(9.0, d1.samletDosis(), 0.01);

	}

	@Test
	public void TestSamletDosis4() {
		DagligFast d1 = new DagligFast(LocalDate.of(2020, 01, 8), LocalDate.of(2020, 01, 12), 1, 1, 1, 1);

		assertEquals(20.0, d1.samletDosis(), 0.01);

	}

}
