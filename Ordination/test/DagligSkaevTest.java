package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;

public class DagligSkaevTest {

	@Test
	public void opretDosisTest() {
		LocalTime[] x = { LocalTime.of(16, 30) };
		double[] antal = { 0.15 };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 02, 20), x, antal);

		assertNotNull(d1);
		assertEquals(LocalDate.of(2020, 01, 20), d1.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 20), d1.getSlutDen());
		assertEquals(0.15, d1.doegnDosis(), 0.01);

	}

	@Test
	public void opretDosisTest2() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 0.15 };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 02, 20), x, antal);

		assertNotNull(d1);
		assertEquals(LocalDate.of(2020, 01, 20), d1.getStartDen());
		assertEquals(LocalDate.of(2020, 02, 20), d1.getSlutDen());
		assertEquals(0.15, d1.doegnDosis(), 0.01);

	}

	@Test
	public void samletDosisTest() {
		LocalTime[] x = { LocalTime.of(16, 30) };
		double[] antal = { 0.15, };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(0.30, d1.samletDosis(), 0.01);

	}

	@Test
	public void samletDosisTest2() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 0.15, };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(0.30, d1.samletDosis(), 0.01);

	}

	@Test
	public void doegnDosisTest() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 2 };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(2, d1.doegnDosis(), 0.01);

	}

	@Test
	public void doegnDosisTest1() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 2 };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 30), x, antal);

		assertNotNull(d1);
		assertEquals(2, d1.doegnDosis(), 0.01);

	}

	@Test
	public void doegnDosisTest2() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 100, };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(100, d1.doegnDosis(), 0.01);

	}

	@Test
	public void doegnDosisTest3() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { 0, };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(0, d1.doegnDosis(), 0.01);

	}

	@Test
	public void doegnDosisTest4() {
		LocalTime[] x = { LocalTime.of(00, 00) };
		double[] antal = { -1, };
		DagligSkaev d1 = new DagligSkaev(LocalDate.of(2020, 01, 20), LocalDate.of(2020, 01, 21), x, antal);

		assertNotNull(d1);
		assertEquals(-1, d1.doegnDosis(), 0.01);

	}

}
