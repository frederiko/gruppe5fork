package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class PNTest {

	@Before
	public void setUp() throws Exception {
		Patient p = new Patient("121256-0512", "Jane Jensen", 63.4);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	public void testSamletDosis() {
		PN pn = new PN(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 02, 02), 2);
		pn.givDosis(LocalDate.of(2020, 02, 01));
		pn.givDosis(LocalDate.of(2020, 02, 02));
		assertEquals(4, pn.samletDosis(), 0.001);

		PN pn1 = new PN(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 02, 01), 2);
		pn1.givDosis(LocalDate.of(2020, 02, 01));
		assertEquals(2, pn1.samletDosis(), 0.001);

		PN pn2 = new PN(LocalDate.of(2020, 02, 01), LocalDate.of(2020, 02, 05), 2);
		pn2.givDosis(LocalDate.of(2020, 02, 01));
		pn2.givDosis(LocalDate.of(2020, 02, 02));
		pn2.givDosis(LocalDate.of(2020, 02, 03));
		pn2.givDosis(LocalDate.of(2020, 02, 04));
		pn2.givDosis(LocalDate.of(2020, 02, 05));
		assertEquals(10, pn2.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		Patient p = new Patient("121256-0512", "Jane Jensen", 63.4);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		// Test 1

		PN pn = new PN((LocalDate.of(2020, 02, 01)), LocalDate.of(2020, 02, 01), 2);
		pn.givDosis(LocalDate.of(2020, 02, 01));
		assertEquals(2, pn.doegnDosis(), 0.0001);

		// Test 2

		PN pn1 = new PN((LocalDate.of(2020, 02, 01)), LocalDate.of(2020, 02, 02), 2);
		pn1.givDosis(LocalDate.of(2020, 02, 01));
		assertEquals(1, pn1.doegnDosis(), 0.0001);

		// Test 3

		PN pn2 = new PN((LocalDate.of(2020, 02, 01)), LocalDate.of(2020, 02, 03), 3);
		pn2.givDosis(LocalDate.of(2020, 02, 01));
		assertEquals(1, pn1.doegnDosis(), 0.0001);
	}

	@Test
	public void testGivDosis() {
		Patient p = new Patient("121256-0512", "Jane Jensen", 63.4);
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		PN pn = new PN((LocalDate.of(2020, 02, 20)), LocalDate.of(2020, 02, 26), 2);

		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 20)));
		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 21)));
		assertEquals(true, pn.givDosis(LocalDate.of(2020, 02, 22)));
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 04, 20)));
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 01, 20)));

	}

}
