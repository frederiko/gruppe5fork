package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	private Controller controller;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getController();
	}

	@Test
	public void testOpretPNOrdination() {
		Patient p = controller.opretPatient("0110960425", "Mathias Foldager Andersen", 73.3);
		Laegemiddel l = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.20, "Styk");

		// Test 1

		PN pn = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 21), p, l, 1);
		assertNotNull(pn);
		assertEquals(LocalDate.of(2020, 02, 21), pn.getSlutDen());
		assertEquals(1, pn.getAntalEnheder(), 0.001);

		// Test 2

		PN pn1 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p, l, 2);
		assertNotNull(pn1);
		assertEquals(LocalDate.of(2020, 02, 25), pn1.getSlutDen());
		assertEquals(2, pn1.getAntalEnheder(), 0.001);

		// Test 3

		PN pn2 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 29), p, l, 3);
		assertNotNull(pn2);
		assertEquals(LocalDate.of(2020, 02, 29), pn2.getSlutDen());
		assertEquals(3, pn2.getAntalEnheder(), 0.001);

		// Test 4

		PN pn3 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 01), p, l, 4);
		assertNotNull(pn3);
		assertEquals(LocalDate.of(2020, 03, 01), pn3.getSlutDen());
		assertEquals(4, pn3.getAntalEnheder(), 0.001);

		// Test 5

		PN pn4 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 03), p, l, 5);
		assertNotNull(pn4);
		assertEquals(LocalDate.of(2020, 03, 03), pn4.getSlutDen());
		assertEquals(5, pn4.getAntalEnheder(), 0.001);

		// Ugyldig data:

		// Test 6

		// Denne metode tillades og fremkalder ikke en exception, vi anser det dog som
		// ugyldig data.
		PN pn5 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 26), p, l, -1);
		assertNotNull(pn5);
		assertEquals(LocalDate.of(2020, 02, 26), pn5.getSlutDen());
		assertEquals(-1, pn5.getAntalEnheder(), 0.001);

		// Test 7

		try {
			PN pn6 = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 19), p, l, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "START DATO ER EFTER SLUT DATO!");
		}

	}

	@Test
	public void testOpretDagligFastOrdination() {
		Patient p = controller.opretPatient("0110960425", "Mathias Foldager Andersen", 73.3);
		Laegemiddel l = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.20, "Styk");

		// Test 1

		DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 21), p,
				l, 1, 1, 1, 1);
		assertNotNull(df);
		assertEquals(LocalDate.of(2020, 02, 21), df.getSlutDen());
		assertEquals(4, df.doegnDosis(), 0.001);

		// Test 2

		DagligFast df1 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 2, 2, 2, 2);
		assertNotNull(df1);
		assertEquals(LocalDate.of(2020, 02, 25), df1.getSlutDen());
		assertEquals(8, df1.doegnDosis(), 0.001);

		// Test 3

		DagligFast df2 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 3, 2, 2, 1);
		assertNotNull(df2);
		assertEquals(LocalDate.of(2020, 02, 25), df2.getSlutDen());
		assertEquals(8, df2.doegnDosis(), 0.001);

		// Test 4

		DagligFast df3 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 29), p,
				l, 3, 2, 2, 1);
		assertNotNull(df3);
		assertEquals(LocalDate.of(2020, 02, 29), df3.getSlutDen());
		assertEquals(8, df3.doegnDosis(), 0.001);

		// Test 5

		DagligFast df4 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 01), p,
				l, 1, 0, 0, 4);
		assertNotNull(df4);
		assertEquals(LocalDate.of(2020, 03, 01), df4.getSlutDen());
		assertEquals(5, df4.doegnDosis(), 0.001);

		// Test 6

		DagligFast df5 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 03), p,
				l, 2, 1, 4, 3);
		assertNotNull(df5);
		assertEquals(LocalDate.of(2020, 03, 03), df5.getSlutDen());
		assertEquals(10, df5.doegnDosis(), 0.001);

		// Ugyldig data

		// Test 7

		// Denne metode tillades og fremkalder ikke en exception, vi anser det dog som
		// ugyldig data.
		DagligFast df6 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 26), p,
				l, -1, -1, -1, -1);
		assertNotNull(df6);
		assertEquals(LocalDate.of(2020, 02, 26), df6.getSlutDen());
		assertEquals(-4, df6.doegnDosis(), 0.001);

		// Test 8

		try {
			DagligFast df7 = controller.opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
					LocalDate.of(2020, 02, 19), p,
					l, 1, 1, 1, 1);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "START DATO ER EFTER SLUT DATO!");
		}
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		Patient p = controller.opretPatient("0110960425", "Mathias Foldager Andersen", 73.3);
		Laegemiddel l = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.20, "Styk");

		// Test 1

		LocalTime[] klokkeSlet = { LocalTime.of(00, 00), LocalTime.of(14, 00), LocalTime.of(20, 00) };
		double[] antalEnheder = { 2, 3, 2 };

		DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 21),
				p, l, klokkeSlet, antalEnheder);
		assertNotNull(ds);
		assertEquals(LocalDate.of(2020, 02, 21), ds.getSlutDen());
		assertEquals(7, ds.doegnDosis(), 0.001);

		// Test 2

		LocalTime[] klokkeSlet1 = { LocalTime.of(8, 00), LocalTime.of(16, 00), LocalTime.of(23, 00) };
		double[] antalEnheder1 = { 2, 1, 2 };

		DagligSkaev ds1 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25),
				p, l, klokkeSlet1, antalEnheder1);
		assertNotNull(ds1);
		assertEquals(LocalDate.of(2020, 02, 25), ds1.getSlutDen());
		assertEquals(5, ds1.doegnDosis(), 0.001);

		// Test 3

		LocalTime[] klokkeSlet2 = { LocalTime.of(8, 00), LocalTime.of(16, 00), LocalTime.of(22, 00) };
		double[] antalEnheder2 = { 2, 1, 1 };

		DagligSkaev ds2 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 29),
				p, l, klokkeSlet2, antalEnheder2);
		assertNotNull(ds2);
		assertEquals(LocalDate.of(2020, 02, 29), ds2.getSlutDen());
		assertEquals(4, ds2.doegnDosis(), 0.001);

		// Ugyldig data

		// Test 4
		// Denne metode tillades og fremkalder ikke en exception, vi anser det dog som
		// ugyldig data.
		LocalTime[] klokkeSlet3 = { LocalTime.of(8, 00), LocalTime.of(16, 00), LocalTime.of(22, 00) };
		double[] antalEnheder3 = { -2, -1, 1 };

		DagligSkaev ds3 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 26),
				p, l, klokkeSlet3, antalEnheder3);
		assertNotNull(ds3);
		assertEquals(LocalDate.of(2020, 02, 26), ds3.getSlutDen());
		assertEquals(-2, ds3.doegnDosis(), 0.001);

		// Test 5

		LocalTime[] klokkeSlet4 = { LocalTime.of(00, 00), LocalTime.of(14, 00), LocalTime.of(20, 00) };
		double[] antalEnheder4 = { 2, 3 };

		try {
			DagligSkaev ds4 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20),
					LocalDate.of(2020, 02, 22),
					p, l, klokkeSlet4, antalEnheder4);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Ikke lige antal elementer i antalenheder og klokkeslet");
		}

		// Test 6

		LocalTime[] klokkeSlet5 = { LocalTime.of(00, 00), LocalTime.of(14, 00), LocalTime.of(20, 00) };
		double[] antalEnheder5 = { 2, 3, 2 };

		try {
			DagligSkaev ds5 = controller.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20),
					LocalDate.of(2020, 02, 19),
					p, l, klokkeSlet5, antalEnheder5);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "START DATO ER EFTER SLUT DATO!");
		}
	}

	@Test
	public void testOrdinationPNAnvendt() {
		Patient p = controller.opretPatient("0110960425", "Mathias Foldager Andersen", 73.3);
		Laegemiddel l = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.20, "Styk");
		PN pn = controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 26), p, l, 2);

		// Test 1

		controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 20));
		assertEquals(1, pn.getAntalGangeGivet());

		// Test 2

		controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 24));
		assertEquals(2, pn.getAntalGangeGivet());

		// Test 3

		controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 26));
		assertEquals(3, pn.getAntalGangeGivet());

		// Test 4

		try {
			controller.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 19));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "ANGIVNE DATO ER UDENFOR START OG SLUT DATO");
		}
	}

//
	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		Patient p = controller.opretPatient("1", "Jørgen", 60.3);
		Patient p1 = controller.opretPatient("1", "Lars", 70.3);
		Patient p2 = controller.opretPatient("1", "Hans", 100.3);

		Laegemiddel l = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.20, "Styk");

		controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 22), p, l, 2);
		controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 22), p1, l, 1);
		controller.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 22), p2, l, 3);

		// Test 1

		assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(60, 70, l));

		// Test 2

		assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(60, 80, l));

		// Test 3

		assertEquals(3, controller.antalOrdinationerPrVægtPrLægemiddel(60, 150, l));

		// Test 4

		try {
			controller.antalOrdinationerPrVægtPrLægemiddel(50, -1, l);
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), "Slut eller start vægt er mindre end 0");
		}
	}

}