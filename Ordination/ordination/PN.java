package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

//hej

public class PN extends Ordination {

	private double antal;
	ArrayList<LocalDate> registreredeDoser = new ArrayList<>();

	/**
	 * Pre: Dates are not null. Pre: Dates are >=1
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	public PN(LocalDate startDen, LocalDate slutDen, double antal) {
		super(startDen, slutDen);
		this.antal = antal;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		boolean erGivet = false;
		if (!givesDen.isBefore(getStartDen()) && !givesDen.isAfter(getSlutDen())) {
			registreredeDoser.add(givesDen);
			erGivet = true;
		}
		return erGivet;
	}

	/**
	 * Der er oprettet en liste af doser
	 * 
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double doegnDosis() {
		double doser = getAntalEnheder() * getAntalGangeGivet();
		double doegndosis = doser / super.antalDage();
		return doegndosis;
	}

	/**
	 * Der er oprettet en liste af doser
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * getAntalEnheder();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return registreredeDoser.size();
	}

	public double getAntalEnheder() {
		return antal;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
