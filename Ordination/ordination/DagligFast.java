package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	/**
	 * Pre: Dates are not null. Pre: Dates are >=1
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	public DagligFast(LocalDate startDen, LocalDate slutDen, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		super(startDen, slutDen);
		doser[0] = new Dosis(null, morgenAntal);
		doser[1] = new Dosis(null, middagAntal);
		doser[2] = new Dosis(null, aftenAntal);
		doser[3] = new Dosis(null, natAntal);
	}

	/**
	 * Pre: Der er oprettet en liste af doser
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double samletDosis() {
		return super.antalDage() * doegnDosis();

	}

	/**
	 * Pre: Der er oprettet en liste af doser
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double doegnDosis() {
		double doegndosis = 0;
		for (Dosis d : doser) {
			doegndosis += d.getAntal();
		}
		return doegndosis;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] getDoser() {
		return doser;
	}
}
