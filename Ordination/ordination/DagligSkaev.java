package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	/**
	 * Pre: Dates are not null. Pre: Dates are >=1
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	public DagligSkaev(LocalDate startDen, LocalDate slutDen, LocalTime[] klokkeSlet, double[] antalEnheder) {
		super(startDen, slutDen);

		for (int i = 0; i < klokkeSlet.length; i++) {
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
	}

	/**
	 * Pre: Der er oprettet en liste af doser Opretter en dosis med localT
	 * 
	 * @param tid
	 * @param antal
	 */
	public void opretDosis(LocalTime tid, double antal) {
		Dosis d = new Dosis(tid, antal);
		doser.add(d);
	}

	/**
	 * Pre: Der er oprettet en liste af doser
	 * 
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double samletDosis() {
		return doegnDosis() * super.antalDage();
	}

	/**
	 * Pre: Der er oprettet en liste af doser
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	@Override
	public double doegnDosis() {
		double antalDoser = 0;
		for (Dosis dosis : doser) {
			antalDoser += dosis.getAntal();
		}
		return antalDoser;
	}

	@Override
	public String getType() {
		return "DagligSkaev";
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}
}
