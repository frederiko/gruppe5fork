package ordination;

import java.time.LocalTime;

//hej

public class Dosis {
	private LocalTime tid;
	private double antal;

	/**
	 * Pre: tid is not null. Pre Antal is not Null
	 * 
	 * @param startDen
	 * @param slutDen
	 */
	public Dosis(LocalTime tid, double antal) {
		super();
		this.tid = tid;
		this.antal = antal;
	}

	public double getAntal() {
		return antal;
	}

	public void setAntal(double antal) {
		this.antal = antal;
	}

	public LocalTime getTid() {
		return tid;
	}

	public void setTid(LocalTime tid) {
		this.tid = tid;
	}

	@Override
	public String toString() {
		return "Kl: " + tid + "   antal:  " + antal;
	}

}
